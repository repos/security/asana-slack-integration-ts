import copy
import re
import time
from config import Config
from models.message import Message
from models.task import Task
from services.asana_api import AsanaApi
from services.publisher import Publisher
from services.slack_api import SlackApi


class TestPublisher:
    client = SlackApi(Config.SLACK_KEY)
    publisher = Publisher()
    channel = Config.SLACK_DEFAULT_CHANNEL
    mock_task =  Task(
                "1201956028557464", 
                "00031859 - Test task 5", 
                "https://app.asana.com/0/0/1201956028557464", 
                "2022-03-11 16:03:03",
                False, 
                "In progress",
                "Medium",
                "00031859"
            )

    def test_create_id_less_thread(self):
        """We should not publish without ID"""
        task: Task = copy.deepcopy(self.mock_task)
        task.ticket_id = None
        response = self.publisher.create_thread(self.client, self.channel, task)

        assert response.ts is None

    '''
    def test_triage_update(self):
        
        response = self.publisher.triage_update(self.client, self.channel, self.mock_task)

        assert response.ts


    def test_create_thread(self):
        """Test method for Publisher.update_thread()"""
        response = self.publisher.create_thread(self.client, self.channel, self.mock_task)

        assert response.ts

    def test_create_id_less_thread(self):
        """We should not publish without ID"""
        task: Task = copy.deepcopy(self.mock_task)
        task.ticket_id = None
        response = self.publisher.create_thread(self.client, self.channel, task)

        assert response.ts is None


    def test_update_thread(self):
        """Test method for Publisher.update_thread()"""
        updated_task = copy.deepcopy(self.mock_task)
        updated_task.column = "Waiting"
        new_thread = self.publisher.create_thread(self.client, self.channel, self.mock_task)
        
        # Wait a bit
        time.sleep(5)
        
        # Update thread
        response = self.publisher.update_thread(self.client, self.channel, updated_task, new_thread.ts )

        assert response.ts

    '''