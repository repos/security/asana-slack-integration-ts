from config import Config
from models.task import Task
from services.asana_api import AsanaApi

class TestAsanaApi:
    """
    Test suite for operations related to the
    Slack API
    """
    client: AsanaApi = AsanaApi(Config.ASANA_KEY, Config.ASANA_DEFAULT_PROJECT)
    task_params = {
        'name': '0003228 - Test task 5',
        'notes': 'Note: This is a test task created with the python-asana client.',
        'projects': Config.ASANA_DEFAULT_PROJECT
    }



    def get_task_by_id(self):
        """
        Pull data from Asana and format it into
        a Task object for later use
        """

        
        # Create a new task
        new_task: Task = self.client.create_task(self.task_params)
        task_id = new_task["gid"]

        # Fetch task from Asana
        fetched_task = self.client.get_task_by_id(task_id)
            
        assert "title" in fetched_task
        assert fetched_task.title in new_task.title


    def test_create_and_delete_task(self):

        # Create a new task
        new_task = self.client.create_task(self.task_params)
        self.client.delete_task(new_task["gid"])

        assert type(new_task) is dict