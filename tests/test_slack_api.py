from datetime import datetime
import time
from config import Config
from models.message import Message
from models.task import Task
from services.database_manager import DatabaseManager
from services.publisher import Publisher
from services.slack_api import SlackApi

class TestSlackApi:
    """
    Test suite for operations related to the
    Slack API
    """

    def test_post_message(self):
        """
        Testing the post_message() method
        """

        api = SlackApi(Config.SLACK_KEY)
        new_message = "Test message sent at " + str(datetime.now())
        channel = Config.SLACK_DEFAULT_CHANNEL
        new_message = api.post_message(new_message, channel)

        # Delete new message
        api.delete_message(channel, new_message)
                
        assert new_message.ts
  

    def test_post_message_in_thread(self):
        """
        Testing the post_message() method by specifying
        a thread ID
        """

        client = SlackApi(Config.SLACK_KEY)
        channel = Config.SLACK_DEFAULT_CHANNEL
        message = "Test message #1 at " + str(datetime.now())

        # Start a thread with a first message
        message1 = client.post_message(message, channel)
        thread_ts = message1.ts
        
        # Send second message in newly created thread
        message2 = client.post_message("Update sent at " + str(datetime.now()), channel, thread_ts)

        # Delete messages
        client.delete_message(channel, message2)
    
        assert message2.ts

 
    def test_get_parent_threads(self):
        """
        Testing the get_thread_parents() method
        """

        api = SlackApi(Config.SLACK_KEY)
        channel = Config.SLACK_DEFAULT_CHANNEL
        thread_parents = []
        messages = api.get_messages(channel)
        thread_parents = api.get_parent_threads(messages)

        # print("{} messages found in {}".format(len(thread_parents), channel))

        assert len(thread_parents) > 0

    
    def test_delete_message(self):
        """
        Testing the delete_message() method
        """
        api = SlackApi(Config.SLACK_KEY)
        channel = Config.SLACK_DEFAULT_CHANNEL
        
        # Create a new message
        message = "Test message sent at " + str(datetime.now())
        new_message = api.post_message(message, channel)
        
        # Sleep for a while
        time.sleep(2)
        
        # Delete newly created message
        result = api.delete_message(channel, new_message)
        
        assert result == True


    """
    Test case for find_thread_by_ticket_id
    """

    def test_find_thread_by_ticket_id(self):
        client = SlackApi(Config.SLACK_KEY)
        channel = Config.SLACK_DEFAULT_CHANNEL
        task =  Task(
                    "1201956028557464", 
                    "00031859 - Test task 5", 
                    "https://app.asana.com/0/0/1201956028557464", 
                    "2022-03-11 16:03:03",
                    False, 
                    "In progress",
                    "Medium",
                    "00031859"
                )

        publisher = Publisher()
        new_message = publisher.create_thread(client, channel, task)
        new_message.thread_ts = new_message.ts
        
        parent_threads = [
            Message(None, None, "Dummy text 1", None),
            new_message
            ]
        
        ticket_id = task.ticket_id    
        result = client.find_thread_by_ticket_id(parent_threads, ticket_id)
        
        assert result is not False


    def test_slack_api(self):
        from slack import WebClient
        from slack_sdk.web.slack_response import SlackResponse

        response = SlackResponse(
                client=WebClient(token="xoxb-dummy"),
                http_verb="POST",
                api_url="http://localhost:3000/api.test",
                req_args={},
                data={"ok": True, "args": {"hello": "world"}},
                headers={},
                status_code=200,
            )

        assert "ok" in response.data
        assert "args" in response.data
        assert "error" not in response.data
