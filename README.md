# asana-slack-integration

An automated tool for tracking status of tickets in Asana and publishing updates in Slack as threaded messages.

## Requirements
* [Python 3.x+](https://www.python.org/downloads/)
* [PIP (Python Dependency Manager)](https://pip.pypa.io/en/stable/installing/)
* Copy `example.env` to `.env` and adjust your settings accordingly.


## Installing dependencies
Application dependencies can be installed this way:
`pip install -r requirements.txt`.

## Launch the bot
`flask run`

## Unit and integration tests
### Testing
Tests live under the `./tests` repository and are powered by [Pytest](https://docs.pytest.org). To launch a series of unit tests, simply use the command `pytest -v`.
 
 **Note:** make sure to update the `SLACK_DEFAULT_CHANNEL` setting in the  `.env` file and use the ID of a test channel. Slack-specific integration tests are designed to interact and publish test messages in a target channel or group.

### Test coverage report
To generate a coverage report run `coverage run -m pytest` and display the report with `coverage report`

## Maintenance
In case you need to create a regenerate a webhook to watch your task changes, you can make use of the maintenance scripts.

* Use or set `maintenance_server.py` as the entry point of your Flask application. It will be responsible for handling the security handshake operation. 
* Make a note of the public URL of your application as you'll need it for the handshake. If you are testing it locally, take a look at options like [Localtunnel](https://github.com/localtunnel/localtunnel).
* Use your application URL and update the `WEBHOOK_URL` value in your `.env` file.
* Run the `maintenance_create_webhook.py` script with `python maintenance_create_webhook.py`.
* Upon specifc creation of the webhook, replace the entry, stop the server, set `app.py` as the entry point of your Flask application and restart it.

## [License](./LICENSE)

This project is [MIT licensed](./LICENSE).