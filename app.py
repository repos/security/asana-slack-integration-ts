from flask import Flask, request, make_response
from config import Config
from models.task import Task
from services.publisher import Publisher
from services.slack_api import SlackApi
from services.asana_api import AsanaApi
import json

"""
Entry point for the Asana monitoring bot.
It analyzes how tasks change over time and
reports on their statuses in Slack
"""

app = Flask(__name__)
publisher = Publisher()
asana_api = AsanaApi(Config.ASANA_KEY, Config.ASANA_DEFAULT_PROJECT)
slack_api = SlackApi(Config.SLACK_KEY)
channel = Config.SLACK_DEFAULT_CHANNEL


@app.route('/receive-webhook', methods=['POST'])
def receive_hook():
    contents = json.loads(request.data)
    task = None
    if len(contents["events"])<= 0:
        ("", 500)
    
    event = contents["events"][0]    
    task_id =  event["parent"]["gid"]
    
    # Fetch the relevant Asana task based on ID
    task: Task = asana_api.get_task_by_id(str(task_id))
    if task.ticket_id is None:
        ("", 404)
    
    publisher.triage_update(slack_api, channel, task)

    return ("", 200)