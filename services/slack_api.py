import re
import ssl
from typing import List
from models.message import Message
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

class SlackApi:
    """
    Business logic for Slack HTTP operations
    such as posting into threads
    """

    def __init__(self, token: str):
        self.client = WebClient(token)
        pass


    def post_message(self, message: str, channel: str, thread_ts=None) -> Message:
        """
        Publish message in a Slack channel
        """
        try:
            # Deal with message threading
            if thread_ts is None:
                response = self.client.chat_postMessage(
                    channel=channel,
                    text=message
                )
            else:
                
                response=self.client.chat_postMessage(
                    channel=channel,
                    thread_ts=thread_ts,
                    text=message,
                    reply_broadcast=False
                )    
             # Ensure that the response back from Slack
             # is a successful one and that the message is the one we sent
            if response["message"]["text"] == message:
                message = Message(
                    channel,
                    response["message"]["user"],
                    response["message"]["text"],
                    response["message"]["ts"],
                    )
                return message
        
        except:
            pass

        return Message(None, None, None, None)


    def get_messages(self, channel) -> List[Message]:
        """
        Collect a list of messages from channel
        """
        try:
            messages = []
            result = self.client.conversations_history(channel=channel)            
            for message in result["messages"]:
                if "user" in message:
                    messages.append(
                        Message(
                            channel,
                            message["user"],
                            message["text"],
                            message["ts"],
                            None if "thread_ts" not in message else message["thread_ts"]
                        )
                )
        except SlackApiError as e:
            print(e.response)
        
        return messages


    def get_parent_threads(self, messages: List[Message]) -> List[Message]:
        """
        Cherry pick messages that are thread parents
        """
        thread_parents = []
            
        for message in messages:
            if message.thread_ts is not None:
                if message.thread_ts == message.ts:
                    thread_parents.append(message)
                
        return thread_parents


    def delete_message(self, channel: str, message: Message)-> bool:
        """
        Remove a message from the channel
        """
        try:
            result = self.client.chat_delete(channel=channel, ts=message.ts)
            return True
    
        except SlackApiError as e:
            print(f"Error deleting message: {e}")

        return False


    def find_thread_by_ticket_id(self, parent_threads: List[Message], ticket_id: str):
        """ Search for a specific thread """

        ticket_id_regex = r'(' + str(ticket_id) + r')'
        for thread in parent_threads:
            results = re.findall(ticket_id_regex, thread.text)
            if len(results) > 0:
                # print('Ticket {} was found'.format(thread))
                return thread
        
        return False