from typing import List
from asana import Client
import re
from models.task import Task
from config import Config
from datetime import datetime


class AsanaApi:
    """
    Wrapper class for the Asana Client library
    """

    def __init__(self, token, project_id):
        self.client: Client = Client.access_token(token)
        self.default_project_id = project_id


    def get_task_by_id(self, task_id) -> Task:
        """
        Pull data from Asana and format it into
        a Task object for later use
        """

        task: dict = self.client.tasks.find_by_id(task_id)
        if "gid" in task:
            return  Task(
                        task_id,
                        task["name"],
                        "{}".format("https://app.asana.com/0/0/" + task["gid"]),
                        task["completed"],
                        datetime.strftime(
                            datetime.strptime(
                                task["created_at"], "%Y-%m-%dT%H:%M:%S.%fz"
                            ),
                            "%Y-%m-%d %H:%m:%S",
                        ),
                        self.extract_column(task),
                        self.extract_priority(task),
                        self.extract_ticket_id(task),
                    )
        return None


    def extract_ticket_id(self, task):
        """
        Get Investigation ID out of
        the task title
        :param task: Dict to search in
        """

        task_id_regex = r"(\d{4,})"
        results = re.findall(task_id_regex, task["name"])
        if len(results) > 0:
            return results[0]

        return None


    def extract_column(self, task):
        for item in task["memberships"]:
            if item["project"]["gid"] == self.default_project_id:
                return item["section"]["name"]
        return False


    def extract_priority(self, task):
        for item in task["custom_fields"]:
            try:
                if item["name"] == "Priority" and item["display_value"] is not None:
                    return item["display_value"]
            except:
                pass

        return ""

    def create_task(self, params):
        """Create a new task"""

        try:
            return self.client.tasks.create(params)
        except: 
            return False


    def update_task(self, task_id, params):
        """Delete an existing task"""
        try:
            return self.client.tasks.delete(task_id, params)
        except: 
            return False

    def delete_task(self, task_id):
        """Delete an existing task"""
        try:
            return self.client.tasks.delete(task_id)
        except: 
            return False