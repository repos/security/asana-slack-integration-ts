from typing import List, Tuple
from models.message import Message
from models.task import Task
from services.slack_api import SlackApi


class Publisher:
    def __init__(self):
        pass

    def triage_update(self, slack_client: SlackApi, channel: str, task: Task):

        
        parent_threads = slack_client.get_parent_threads(slack_client.get_messages(channel))

        # Chech wether task already exist
        existing_thread: Message = slack_client.find_thread_by_ticket_id(parent_threads, task.ticket_id)
        
        if existing_thread is not False:
            return self.update_thread(slack_client, channel, task, existing_thread.ts)
        
        return self.create_thread(slack_client, channel, task)


    def update_thread(self,
                        slack_client: SlackApi,
                        channel: str,
                        task: Task,
                        thread_ts: str):
        
        # Send second message in newly created thread
        # TODO: decouple text for more flexiblity and i10n purpose.
        message = "{} has progressed to `{}`.".format(task.ticket_id, task.column)
        response = slack_client.post_message(message, channel, thread_ts)
        
        return response


    def create_thread(self, client: SlackApi, channel: str, task: Task) -> Message:
        if(task.ticket_id is not None):
            # Start a new thread with a first message
            thread_initial_message =  "*{}*".format(task.title)
            message_1 = client.post_message(thread_initial_message, channel)
            thread_ts = message_1.ts
            
            # Send second message in newly created thread
            # TODO: decouple text for more flexiblity and i10n purpose.
            message_2 = "{} has progressed to `{}`.".format(task.ticket_id, task.column)
            response = client.post_message(message_2, channel, thread_ts)

            return response

        return Message(None, None, None, None)