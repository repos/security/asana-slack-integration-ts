import os
from decouple import config


class Config(object):
    """
    A set of configurations that will be
    used across the application
    """

    TITLE = config("TITLE")
    DESCRIPTION = config("DESCRIPTION")
    ASANA_KEY = config("ASANA_KEY")
    ASANA_DEFAULT_PROJECT = config("ASANA_DEFAULT_PROJECT")
    ASANA_DEFAULT_WORKSPACE = config("ASANA_DEFAULT_WORKSPACE")
    SLACK_KEY = config("SLACK_KEY")
    SLACK_DEFAULT_CHANNEL = config("SLACK_DEFAULT_CHANNEL")
    WEBHOOK_URL = config("WEBHOOK_URL")
    ROOT_FOLDER = os.path.dirname(os.path.abspath(__file__))
