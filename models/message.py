class Message:
    """
    Abstract data model providing a unified way to represent
    message fetched from slack channels
    """

    def __init__(self, channel: str, user: str, text: str, ts: str, thread_ts=None):
        """
        @param: channel where the message was posted
        @param: user who authored the text
        @param: text the content of the message
        @param: ts Message UUID
        @param: thread_ts Whether the message is a thread
        
        """
        self.channel = channel
        self.user = user
        self.text = text
        self.ts = ts
        self.thread_ts = thread_ts
        

    def __repr__(self):
        return "<Message {} | {} | {} | {} | {}>".format(
            self.channel,
            self.user,
            self.text,
            self.ts,
            self.thread_ts,
        )
