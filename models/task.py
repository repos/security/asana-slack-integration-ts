import json


class Task:
    """
    Abstract data model providing a unified way to represent
    tasks fetched from various providers
    """

    def __init__(self, id, title, link, is_completed, created_at, column=False, priority="", ticket_id=None):
        """
        @param: id Task identifier
        @param: title Title of the task
        @param: link URI for accessing the task
        @param: is_completed Closure status
        @param: created_at When the task was first created
        @param: column Dashboard column where it is located
        @param: priority Level of priority, usually Low to High
        @param: ticket_id Arbitrary ID based on domain knowlegdge
        """
        self.id = id
        self.title = title
        self.link = link
        self.created_at = created_at
        self.is_completed = is_completed
        self.column = column
        self.priority = priority
        self.ticket_id = ticket_id

    @classmethod
    def from_dict(cls, json_dict):
        return cls(**json_dict)

    def to_dict(self):
        return {
                "id": self.id,
                "title": self.title,
                "link": self.link,
                "created_at": self.created_at,
                "is_completed": self.is_completed,
                "column": self.column,
                "priority": self.priority,
                "ticket_id": self.ticket_id,
            }

    def __repr__(self):
        return "<Task {} | {} | {} | {} | {} | {} | {} | {}>".format(
            self.id,
            self.title,
            self.link,
            self.created_at,
            self.is_completed,
            self.column,
            self.priority,
            self.ticket_id,
        )
