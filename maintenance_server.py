from flask import Flask, request, make_response
import json

app = Flask(__name__)


@app.route('/receive-webhook', methods=['POST'])
def handle_handshake():
    """
    The following route handles the webhook handshake process
    and verifies that a valid secret key was provided.
    """
    hook_secret = None
    if "X-Hook-Secret" in request.headers:
        if hook_secret is not None:
            app.logger.info("Second handshake request received. This could be an attacker trying to set up a new secret. Ignoring.")
            response = make_response("", 200)
        else:
            # Respond to the handshake request :)
            app.logger.info("New webhook")
            response = make_response("", 200)

            # Save the secret for later to verify incoming webhooks
            hook_secret = request.headers["X-Hook-Secret"]
            response.headers["X-Hook-Secret"] = request.headers["X-Hook-Secret"]
            return response

    elif "X-Hook-Signature" in request.headers:
        # Signature sent by Asana's API should match the one calculated locally.
        
        contents = json.loads(request.data)
        app.logger.warn("Received payload of %s events \n", len(contents["events"]))
        
    return ""

