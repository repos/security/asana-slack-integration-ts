from config import Config
from services.asana_api import AsanaApi
import json

asana = AsanaApi(Config.ASANA_KEY, Config.ASANA_DEFAULT_PROJECT)
webhook_settings = {
    "filters": [
    {
        "action": "added", 
        "resource_type": "story",
        "resource_subtype": "section_changed"
    }
    ],
    "resource": Config.ASANA_DEFAULT_PROJECT,
    "target": Config.WEBHOOK_URL
}


def create_webhook():
    try:
        webhook = asana.client.webhooks.create(webhook_settings)
        print(json.dumps(webhook))
    except Exception as e:
        print(str(e))


def reset_hooks():
    # arbitrary deletion of existing hooks
    webhooks = list(asana.client.webhooks.get_all(workspace=Config.ASANA_DEFAULT_WORKSPACE))
    for hook in webhooks:
        asana.client.webhooks.delete_by_id(str(hook["gid"]))


# Reset webhooks, optionally and create a new one
reset_webhooks = True

if reset_webhooks is True:
    reset_hooks()

# Create a new webhook
create_webhook()